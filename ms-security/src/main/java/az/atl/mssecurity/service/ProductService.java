package az.atl.mssecurity.service;

import az.atl.mssecurity.client.ProductClient;
import az.atl.mssecurity.model.client.ProductDto;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class ProductService {
    private final ProductClient productClient;

    public List<ProductDto> getAllProducts(){
        return productClient.getAllProducts();
    }
}
