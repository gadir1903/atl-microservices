package az.atl.mssecurity.model;


import az.atl.mssecurity.dao.entity.User;
import lombok.*;
import lombok.experimental.FieldDefaults;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class RegisterResponse {
    String fullName;
    String email;
    String password;
    String phoneNumber;
    Role role;


    public static RegisterResponse buildRegisterDto(User user){
        return RegisterResponse.builder()
                .fullName(user.getFullName())
                .email(user.getEmail())
                .password(user.getPassword())
                .phoneNumber(user.getPhoneNumber())
                .role(user.getRole())
                .build();
    }
}
