package az.atl.msproduct.service;

import az.atl.msproduct.dao.entity.ProductEntity;
import az.atl.msproduct.dao.repository.ProductRepo;
import az.atl.msproduct.exception.ProductNotFoundException;
import az.atl.msproduct.model.dto.ProductDto;
import az.atl.msproduct.service.impl.ProductServiceImpl;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@SpringBootTest
public class ProductServiceImplTest {
    @Mock
    private ProductRepo productRepo;
    @InjectMocks
    private ProductServiceImpl productService;

    @Test
    void testGetProductById_withTrueCase() {
        var productEntity = ProductEntity.builder()
                .id(1L)
                .name("name")
                .count(12)
                .phoneNumber("+994501234567")
                .build();
        var productDto = ProductDto.builder()
                .id(1L)
                .name("name")
                .count(12)
                .phoneNumber("+994501****67")
                .build();
        when(productRepo.findById(1L)).thenReturn(Optional.of(productEntity));

        var result = productService.getProductById(1L);

        assertEquals(productDto, result);
        assertEquals(productDto.getId(), result.getId());
        assertEquals(productDto.getPhoneNumber(), result.getPhoneNumber());
    }

    @Test
    void testGetProductById_withNotFoundException() {

        when(productRepo.findById(any())).thenReturn(Optional.empty());

        assertThrows(ProductNotFoundException.class, () -> productService.getProductById(any()));

    }
    @Test
    public void testCreateProduct(){
        ProductDto productDto = ProductDto.builder()
                .name("product")
                .count(12)
                .build();
        ProductEntity savedEntity = ProductEntity.builder()
                .id(1L)
                .name("product")
                .count(12)
                .build();
        when(productRepo.save(any(ProductEntity.class))).thenReturn(savedEntity);

        long createdId = productService.createProduct(productDto);

        assertEquals(1L,createdId);
        verify(productRepo,times(1)).save(any(ProductEntity.class));
    }
    @Test
    public void testDeleteProduct(){
        Long productId = 1L;
        productService.deleteProductById(productId);
        verify(productRepo,times(1)).deleteById(productId);
    }
}
