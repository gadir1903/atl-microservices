package az.atl.msproduct.util;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class PhoneNumberMaskUtilTest {
    @Test
    void testMaskPhoneNumber_withTrueCase() {
        var phoneNumber = "+994501234567";
        var expected = "+994501****67";
        var result = PhoneNumberMaskUtil.maskPhoneNumber(phoneNumber);
        assertEquals(expected, result);
    }

    @Test
    void testMaskPhoneNumber_withNullValue() {
        String phoneNumber = null;
        assertThrows(IllegalArgumentException.class, () -> PhoneNumberMaskUtil.maskPhoneNumber(phoneNumber));
    }

    @Test
    void testMaskPhoneNumber_withLength() {
        String phoneNumber = "+99450";
        assertThrows(IllegalArgumentException.class, () -> PhoneNumberMaskUtil.maskPhoneNumber(phoneNumber));
    }
}
