package az.atl.msproduct.model;

import az.atl.msproduct.dao.entity.ProductEntity;
import az.atl.msproduct.model.dto.ProductDto;
import az.atl.msproduct.model.mapper.ProductMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static az.atl.msproduct.util.PhoneNumberMaskUtil.maskPhoneNumber;

public class ProductMapperTest {
    @Test
    public void testMapEntToDto() {
        ProductEntity entity = new ProductEntity();
        entity.setId(1L);
        entity.setName("product");
        entity.setPhoneNumber("+994501234567");

        ProductDto dto = ProductMapper.PRODUCT_MAPPER.buildDto(entity);

        Assertions.assertEquals(entity.getId(), dto.getId());
        Assertions.assertEquals(entity.getName(), dto.getName());
        Assertions.assertNotEquals(entity.getPhoneNumber(), maskPhoneNumber(dto.getPhoneNumber()));
    }

    @Test
    public void testMapDtoToEnt() {
        ProductDto dto = ProductDto.builder()
                .id(1L)
                .name("product")
                .phoneNumber("+994501234567")
                .build();

        ProductEntity entity = ProductMapper.PRODUCT_MAPPER.buildEntity(dto);

        Assertions.assertEquals(dto.getId(), entity.getId());
        Assertions.assertEquals(dto.getName(), entity.getName());
        Assertions.assertEquals(dto.getPhoneNumber(), entity.getPhoneNumber());
    }

}
