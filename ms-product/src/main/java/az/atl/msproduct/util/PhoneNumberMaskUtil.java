package az.atl.msproduct.util;

public class PhoneNumberMaskUtil {
    private static final String MASK_CHARACTER = "*";
    private static final int VISIBLE_DIGITS_START_INDEX = 7;
    private static final int VISIBLE_DIGITS_END_INDEX = 2;
    private static final int VISIBLE_DIGITS_ALL = 13;

    public static String maskPhoneNumber(String phoneNumber){
        if (phoneNumber == null || phoneNumber.length() < VISIBLE_DIGITS_ALL){
            throw new IllegalArgumentException("Invalid Phone Number");
        }
        String visibleDigits = phoneNumber.substring(VISIBLE_DIGITS_START_INDEX,phoneNumber.length() - VISIBLE_DIGITS_END_INDEX);
        String maskedDigits = MASK_CHARACTER.repeat(visibleDigits.length());

        return phoneNumber.substring(0, VISIBLE_DIGITS_START_INDEX) + maskedDigits + phoneNumber.substring(phoneNumber.length() - VISIBLE_DIGITS_END_INDEX);

    }
}
