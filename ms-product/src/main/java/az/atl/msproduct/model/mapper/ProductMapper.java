package az.atl.msproduct.model.mapper;

import az.atl.msproduct.dao.entity.ProductEntity;
import az.atl.msproduct.model.dto.ProductDto;

import static az.atl.msproduct.util.PhoneNumberMaskUtil.maskPhoneNumber;

public enum ProductMapper {
    PRODUCT_MAPPER;
    public final ProductEntity buildEntity(ProductDto productDto) {
        return ProductEntity.builder()
                .id(productDto.getId())
                .name(productDto.getName())
                .phoneNumber(productDto.getPhoneNumber())
                .price(productDto.getPrice())
                .count(productDto.getCount())
                .build();
    }

    public final ProductDto buildDto(ProductEntity entity) {
        return ProductDto.builder()
                .id(entity.getId())
                .name(entity.getName())
                .phoneNumber(maskPhoneNumber(entity.getPhoneNumber()))
                .price(entity.getPrice())
                .count(entity.getCount())
                .build();
    }

}
