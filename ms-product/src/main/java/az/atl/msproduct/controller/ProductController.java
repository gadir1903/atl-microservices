package az.atl.msproduct.controller;


import az.atl.msproduct.dao.entity.ProductEntity;
import az.atl.msproduct.model.dto.ProductDto;
import az.atl.msproduct.service.ProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequiredArgsConstructor
@RequestMapping("/product")
public class ProductController {

    private final ProductService productService;

    @GetMapping("/allProducts")
    @ResponseStatus(HttpStatus.OK)
    public List<ProductDto> getAllProducts() {
        return productService.getAllProducts();
    }

    @GetMapping("/getProductById")
    @ResponseStatus(HttpStatus.OK)
    public ProductDto getProductById(@RequestParam("id") Long id) {
        return productService.getProductById(id);
    }

    @PostMapping("/createProduct")
    @ResponseStatus(HttpStatus.CREATED)
    public long createProduct(@RequestBody ProductDto productDto) {
      return productService.createProduct(productDto);
    }

    @DeleteMapping("/deleteProductById/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void deleteProductById(@PathVariable Long id){
        productService.deleteProductById(id);
    }

    @PutMapping("/update-product/{id}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public ProductDto updateProduct(@PathVariable Long id , @RequestBody ProductEntity productEntity){
        return productService.updateProduct(id , productEntity);
    }
}
