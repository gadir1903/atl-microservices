package az.atl.msproduct.service;


import az.atl.msproduct.dao.entity.ProductEntity;
import az.atl.msproduct.model.dto.ProductDto;

import java.util.List;
import java.util.Optional;

public interface ProductService {
    List<ProductDto> getAllProducts();
    ProductDto updateProduct(Long id, ProductEntity productEntity);
    void deleteProductById(Long id);

    long createProduct(ProductDto productDto);

    ProductDto getProductById(Long id);

}
