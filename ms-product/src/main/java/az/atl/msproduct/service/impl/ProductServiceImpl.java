package az.atl.msproduct.service.impl;

import az.atl.msproduct.dao.entity.ProductEntity;
import az.atl.msproduct.dao.repository.ProductRepo;
import az.atl.msproduct.exception.ProductNotFoundException;
import az.atl.msproduct.model.dto.ProductDto;
import az.atl.msproduct.service.ProductService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

import static az.atl.msproduct.model.mapper.ProductMapper.PRODUCT_MAPPER;


@Service
@AllArgsConstructor
@Slf4j
public class ProductServiceImpl implements ProductService {
    private final ProductRepo productRepo;


    @Override
    public List<ProductDto> getAllProducts() {
        log.warn("getAllProducts.Start");
        var productList = productRepo.findAll().stream().map(PRODUCT_MAPPER::buildDto).toList();
        return productList;
    }

    @Override
    public ProductDto updateProduct(Long id, ProductEntity productEntity) {
        ProductEntity productEnt = productRepo.findById(id).get();
        productEnt.setName(productEntity.getName());
        productEnt.setCount(productEntity.getCount());
        productEnt.setPrice(productEntity.getPrice());
        return PRODUCT_MAPPER.buildDto(productEnt);
    }

    @Override
    public void deleteProductById(Long id) {
        productRepo.deleteById(id);
    }

    @Override
    public long createProduct(ProductDto productDto) {
        var productEntity = PRODUCT_MAPPER.buildEntity(productDto);
        var saveEnt = productRepo.save(productEntity);
        return saveEnt.getId();
    }

    @Override
    public ProductDto getProductById(Long id) {
        var productEntity = productRepo.findById(id).orElseThrow(
                () -> {
                    throw new ProductNotFoundException(("Product Not Found"));
                }
        );
        return PRODUCT_MAPPER.buildDto(productEntity);
    }
}
